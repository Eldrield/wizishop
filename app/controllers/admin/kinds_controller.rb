class Admin::KindsController < ApplicationController
  before_action :set_admin_kind, only: [:show, :edit, :update, :destroy]

  # GET /admin/kinds
  # GET /admin/kinds.json
  def index
    @admin_kinds = Admin::Kind.all
  end

  # GET /admin/kinds/1
  # GET /admin/kinds/1.json
  def show
  end

  # GET /admin/kinds/new
  def new
    @admin_kind = Admin::Kind.new
  end

  # GET /admin/kinds/1/edit
  def edit
  end

  # POST /admin/kinds
  # POST /admin/kinds.json
  def create
    @admin_kind = Admin::Kind.new(admin_kind_params)

    respond_to do |format|
      if @admin_kind.save
        format.html { redirect_to @admin_kind, notice: 'Kind was successfully created.' }
        format.json { render :show, status: :created, location: @admin_kind }
      else
        format.html { render :new }
        format.json { render json: @admin_kind.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/kinds/1
  # PATCH/PUT /admin/kinds/1.json
  def update
    respond_to do |format|
      if @admin_kind.update(admin_kind_params)
        format.html { redirect_to @admin_kind, notice: 'Kind was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_kind }
      else
        format.html { render :edit }
        format.json { render json: @admin_kind.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/kinds/1
  # DELETE /admin/kinds/1.json
  def destroy
    @admin_kind.destroy
    respond_to do |format|
      format.html { redirect_to admin_kinds_url, notice: 'Kind was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_kind
      @admin_kind = Admin::Kind.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def admin_kind_params
      params.require(:admin_kind).permit(:name, :admin_thematic_id)
    end
end
