class Admin::ThematicsController < ApplicationController
  before_action :set_admin_thematic, only: [:show, :edit, :update, :destroy]

  # GET /admin/thematics
  # GET /admin/thematics.json
  def index
    @admin_thematics = Admin::Thematic.all
  end

  # GET /admin/thematics/1
  # GET /admin/thematics/1.json
  def show
  end

  # GET /admin/thematics/new
  def new
    @admin_thematic = Admin::Thematic.new
  end

  # GET /admin/thematics/1/edit
  def edit
  end

  # POST /admin/thematics
  # POST /admin/thematics.json
  def create
    @admin_thematic = Admin::Thematic.new(admin_thematic_params)

    respond_to do |format|
      if @admin_thematic.save
        format.html { redirect_to @admin_thematic, notice: 'Thematic was successfully created.' }
        format.json { render :show, status: :created, location: @admin_thematic }
      else
        format.html { render :new }
        format.json { render json: @admin_thematic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/thematics/1
  # PATCH/PUT /admin/thematics/1.json
  def update
    respond_to do |format|
      if @admin_thematic.update(admin_thematic_params)
        format.html { redirect_to @admin_thematic, notice: 'Thematic was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_thematic }
      else
        format.html { render :edit }
        format.json { render json: @admin_thematic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/thematics/1
  # DELETE /admin/thematics/1.json
  def destroy
    @admin_thematic.destroy
    respond_to do |format|
      format.html { redirect_to admin_thematics_url, notice: 'Thematic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_thematic
      @admin_thematic = Admin::Thematic.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def admin_thematic_params
      params.require(:admin_thematic).permit(:name)
    end
end
