json.extract! admin_article, :id, :name, :description, :price, :feature, :admin_category_id, :created_at, :updated_at
json.url admin_article_url(admin_article, format: :json)
