json.extract! admin_kind, :id, :name, :admin_thematic_id, :created_at, :updated_at
json.url admin_kind_url(admin_kind, format: :json)
