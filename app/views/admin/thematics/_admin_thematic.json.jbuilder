json.extract! admin_thematic, :id, :name, :created_at, :updated_at
json.url admin_thematic_url(admin_thematic, format: :json)
