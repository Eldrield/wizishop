Rails.application.routes.draw do
  namespace :admin do
    resources :articles
  end
  namespace :admin do
    resources :categories
  end
  namespace :admin do
    resources :kinds
  end
  namespace :admin do
    resources :thematics
  end
  devise_for :admins
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
