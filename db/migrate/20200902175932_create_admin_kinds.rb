class CreateAdminKinds < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_kinds do |t|
      t.string :name
      t.references :admin_thematic, null: false, foreign_key: true

      t.timestamps
    end
  end
end
