class CreateAdminCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_categories do |t|
      t.string :name
      t.string :description
      t.references :admin_kind, null: false, foreign_key: true

      t.timestamps
    end
  end
end
