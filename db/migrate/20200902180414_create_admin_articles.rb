class CreateAdminArticles < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_articles do |t|
      t.string :name
      t.string :description
      t.float :price
      t.string :feature
      t.references :admin_category, null: false, foreign_key: true

      t.timestamps
    end
  end
end
