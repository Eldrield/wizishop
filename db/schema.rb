# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_09_02_180414) do

  create_table "admin_articles", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.float "price"
    t.string "feature"
    t.integer "admin_category_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["admin_category_id"], name: "index_admin_articles_on_admin_category_id"
  end

  create_table "admin_categories", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.integer "admin_kind_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["admin_kind_id"], name: "index_admin_categories_on_admin_kind_id"
  end

  create_table "admin_kinds", force: :cascade do |t|
    t.string "name"
    t.integer "admin_thematic_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["admin_thematic_id"], name: "index_admin_kinds_on_admin_thematic_id"
  end

  create_table "admin_thematics", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  add_foreign_key "admin_articles", "admin_categories"
  add_foreign_key "admin_categories", "admin_kinds"
  add_foreign_key "admin_kinds", "admin_thematics"
end
