require 'test_helper'

class Admin::KindsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_kind = admin_kinds(:one)
  end

  test "should get index" do
    get admin_kinds_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_kind_url
    assert_response :success
  end

  test "should create admin_kind" do
    assert_difference('Admin::Kind.count') do
      post admin_kinds_url, params: { admin_kind: { admin_thematic_id: @admin_kind.admin_thematic_id, name: @admin_kind.name } }
    end

    assert_redirected_to admin_kind_url(Admin::Kind.last)
  end

  test "should show admin_kind" do
    get admin_kind_url(@admin_kind)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_kind_url(@admin_kind)
    assert_response :success
  end

  test "should update admin_kind" do
    patch admin_kind_url(@admin_kind), params: { admin_kind: { admin_thematic_id: @admin_kind.admin_thematic_id, name: @admin_kind.name } }
    assert_redirected_to admin_kind_url(@admin_kind)
  end

  test "should destroy admin_kind" do
    assert_difference('Admin::Kind.count', -1) do
      delete admin_kind_url(@admin_kind)
    end

    assert_redirected_to admin_kinds_url
  end
end
