require 'test_helper'

class Admin::ThematicsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_thematic = admin_thematics(:one)
  end

  test "should get index" do
    get admin_thematics_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_thematic_url
    assert_response :success
  end

  test "should create admin_thematic" do
    assert_difference('Admin::Thematic.count') do
      post admin_thematics_url, params: { admin_thematic: { name: @admin_thematic.name } }
    end

    assert_redirected_to admin_thematic_url(Admin::Thematic.last)
  end

  test "should show admin_thematic" do
    get admin_thematic_url(@admin_thematic)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_thematic_url(@admin_thematic)
    assert_response :success
  end

  test "should update admin_thematic" do
    patch admin_thematic_url(@admin_thematic), params: { admin_thematic: { name: @admin_thematic.name } }
    assert_redirected_to admin_thematic_url(@admin_thematic)
  end

  test "should destroy admin_thematic" do
    assert_difference('Admin::Thematic.count', -1) do
      delete admin_thematic_url(@admin_thematic)
    end

    assert_redirected_to admin_thematics_url
  end
end
