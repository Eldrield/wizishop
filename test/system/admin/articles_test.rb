require "application_system_test_case"

class Admin::ArticlesTest < ApplicationSystemTestCase
  setup do
    @admin_article = admin_articles(:one)
  end

  test "visiting the index" do
    visit admin_articles_url
    assert_selector "h1", text: "Admin/Articles"
  end

  test "creating a Article" do
    visit admin_articles_url
    click_on "New Admin/Article"

    fill_in "Admin category", with: @admin_article.admin_category_id
    fill_in "Description", with: @admin_article.description
    fill_in "Feature", with: @admin_article.feature
    fill_in "Name", with: @admin_article.name
    fill_in "Price", with: @admin_article.price
    click_on "Create Article"

    assert_text "Article was successfully created"
    click_on "Back"
  end

  test "updating a Article" do
    visit admin_articles_url
    click_on "Edit", match: :first

    fill_in "Admin category", with: @admin_article.admin_category_id
    fill_in "Description", with: @admin_article.description
    fill_in "Feature", with: @admin_article.feature
    fill_in "Name", with: @admin_article.name
    fill_in "Price", with: @admin_article.price
    click_on "Update Article"

    assert_text "Article was successfully updated"
    click_on "Back"
  end

  test "destroying a Article" do
    visit admin_articles_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Article was successfully destroyed"
  end
end
