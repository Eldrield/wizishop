require "application_system_test_case"

class Admin::KindsTest < ApplicationSystemTestCase
  setup do
    @admin_kind = admin_kinds(:one)
  end

  test "visiting the index" do
    visit admin_kinds_url
    assert_selector "h1", text: "Admin/Kinds"
  end

  test "creating a Kind" do
    visit admin_kinds_url
    click_on "New Admin/Kind"

    fill_in "Admin thematic", with: @admin_kind.admin_thematic_id
    fill_in "Name", with: @admin_kind.name
    click_on "Create Kind"

    assert_text "Kind was successfully created"
    click_on "Back"
  end

  test "updating a Kind" do
    visit admin_kinds_url
    click_on "Edit", match: :first

    fill_in "Admin thematic", with: @admin_kind.admin_thematic_id
    fill_in "Name", with: @admin_kind.name
    click_on "Update Kind"

    assert_text "Kind was successfully updated"
    click_on "Back"
  end

  test "destroying a Kind" do
    visit admin_kinds_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Kind was successfully destroyed"
  end
end
