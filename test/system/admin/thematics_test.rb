require "application_system_test_case"

class Admin::ThematicsTest < ApplicationSystemTestCase
  setup do
    @admin_thematic = admin_thematics(:one)
  end

  test "visiting the index" do
    visit admin_thematics_url
    assert_selector "h1", text: "Admin/Thematics"
  end

  test "creating a Thematic" do
    visit admin_thematics_url
    click_on "New Admin/Thematic"

    fill_in "Name", with: @admin_thematic.name
    click_on "Create Thematic"

    assert_text "Thematic was successfully created"
    click_on "Back"
  end

  test "updating a Thematic" do
    visit admin_thematics_url
    click_on "Edit", match: :first

    fill_in "Name", with: @admin_thematic.name
    click_on "Update Thematic"

    assert_text "Thematic was successfully updated"
    click_on "Back"
  end

  test "destroying a Thematic" do
    visit admin_thematics_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Thematic was successfully destroyed"
  end
end
